<?php

namespace App\Http\Controllers;

use App\Category;
use App\Chapter;
use App\Lesson;
use App\Serie;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('/home');
    }

    public function dashboard()
    {
        $series     = Serie::orderBy('created_at','desc')->get();
        $chapters   = Chapter::orderBy('created_at','desc')->get();
        $categories = Category::orderBy('created_at','desc')->get();
        $lessons    = Lesson::orderBy('created_at','desc')->get();
        return view('dashboard',compact('series','chapters','categories','lessons'));
    }
}
