<?php

namespace App\Http\Controllers;

use App\Chapter;
use App\Lesson;
use App\Serie;
use FFMpeg\FFMpeg;
use FFMpeg\FFProbe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use JD\Cloudder\Facades\Cloudder;
use Session;

class LessonController extends Controller
{
    public function index($formation_slug, $chapter_slug)
    {
        $serie     = Serie::where('slug', $formation_slug)->first();
        $chapter   = Chapter::where('slug', $chapter_slug)->first();
        return view('instructors.lessons.index',compact('serie','chapter'));
    }

    public function create($formation_slug, $chapter_slug)
    {
        $serie     = Serie::where('slug', $formation_slug)->first();
        $chapter   = Chapter::where('slug', $chapter_slug)->first();
        return view('instructors.lessons.create',compact('serie','chapter'));
    }


    public function store(Request $request,$formation_slug, $chapter_slug)
    {
        $serie          =   Serie::where('slug',$formation_slug)->first();
        $chapter        =   Chapter::where('slug',$chapter_slug)->first();
        $this->validate($request,[
            'video_url'      => 'required',
            'title'          => 'required',
            'episode_number' => 'required'
        ]);

        $video        = request()->file('video_url');
        $setvideoName = time().'.'.$video->getClientOriginalExtension();
        //$filePath     = 'formations/'.$serie->title;
        Storage::disk('s3')->put($setvideoName,  file_get_contents($video), 'public');

        Lesson::create([
            'serie_id'          => $serie->id,
            'title'             => $request->title,
            'slug'              => str_slug($request->title),
            'section_id'        => $chapter->id,
            'chapter_id'        => $chapter->id,
            'episode_number'    => $request->episode_number,
            'is_completed'      => false,
            'video_url'         => Storage::disk('s3')->url($setvideoName),
            'video_length'      => 0,
            'description'       => $setvideoName,
            'video_id'          => 0,
        ]);
        Session::flash('success', 'Ajouter Avec Success !!!');
        return redirect()->route('lessons.index',[$formation_slug, $chapter_slug]);
    }

    public function edit($slug)
    {
        $lesson          = Lesson::where('slug',$slug)->first();
        $series          = Serie::orderBy('created_at')->get();
        return view('instructors.lessons.edit', compact('lesson','series'));
    }

    public function update(Request $request, $id)
    {
        $lesson              = Lesson::findOrFail($id);
        $video_url ='';
        if ($request->hasFile('image_url')) {
            Cloudder::delete($lesson->video_url);
            $lesson->video_url     = $request->file('video_url')->getClientOriginalName();
            $video_url             = uploadFile($request->video_url);
            $lesson->video_url     = $video_url;
        }
        $lesson->fill($request->except('video_url'))->save();
        Session::flash('success', 'Modifié avec Succès!');
        return redirect()->route('lessons.index');
    }

    public function delete($formation_slug, $chapter_slug,$id)
    {
        $lesson     = Lesson::find($id);
        if(Storage::disk('s3')->exists($lesson->description)) {
           Storage::disk('s3')->delete($lesson->description);
        }
        $lesson->delete();
        Session::flash('success', 'Supprimé avec Succès!');
        return redirect()->route('lessons.index',[$formation_slug, $chapter_slug]);
    }
}
