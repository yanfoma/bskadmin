<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Session;

class CategoryController extends Controller
{
    public function index(){

        $categories = Category::paginate(2);
        return view('instructors.categories.index',compact('categories'));
    }

    public function create(){
        return view('instructors.categories.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required'
        ]);
        Category::create(['name' => $request->name]);
        Session::flash('success', 'Creer Avec Success !!!');
        return redirect()->route('categories.index');
    }

    public function edit($id)
    {
        $category = Category::find($id);
        return view('instructors.categories.edit')->with('category', $category);

    }
    public function update(Request $request, $id)
    {
        $category       = Category::find($id);
        $category->name = $request->name;
        $category->save();
        Session::flash('success', 'Modifié avec Succès!');
        return redirect()->route('categories.index');
    }

    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
        Session::flash('success', 'Supprimé avec Succès!');
        return redirect()->route('categories.index');
    }
}
