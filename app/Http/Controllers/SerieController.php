<?php

namespace App\Http\Controllers;

use App\Category;
use App\Serie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use JD\Cloudder\Facades\Cloudder;
use Session;

class SerieController extends Controller
{
    public function index(){
        $categories = Category::orderBy('created_at','desc')->get();
        $series     = Serie::orderBy('created_at','desc')->get();
        return view('instructors.series.index',compact('categories','series'));
    }

    public function create(){
        $categories = Category::all();
        return view('instructors.series.create',compact('categories'));
    }

    public function show($slug)
    {
        $serie          = Serie::where('slug',$slug)->first();
        return view('instructors.series.show', compact('serie'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'image_url'   => 'required|mimes:jpeg,bmp,jpg,png|between:1, 2048',
            'title'       => 'required',
            'category_id' => 'required',
            'description' => 'required',
            'type'        => 'required',
            'prix'        => 'required'
        ]);

        if($request->status == 'on'){
            $isPublished = 1;
        }else{
            $isPublished = 0;
        }

        $category           =   Category::find($request->category_id);
        Serie::create([
            'title'         => $request->title,
            'slug'          => str_slug($request->title),
            'image_url'     => uploadFile($request->image_url),
            'category_id'   => $category->id,
            'description'   => $request->description,
            'requirements'  => $request->requirements,
            'type'          => $request->type,
            'price'         => $request->prix,
            'user_id'       => Auth::user()->id,
            'isPublished'   => $isPublished

        ]);

        //if En Conference generer le lien de la conference
          

        Session::flash('success', 'Ajouter Avec Success !!!');
        return redirect()->route('formations.show');
    }
    public function edit($slug)
    {
        $serie          = Serie::where('slug',$slug)->first();
        $categories     = Category::all();
        return view('instructors.series.edit', compact('serie','categories'));
    }
    public function publish($id)
    {
        $serie              = Serie::findOrFail($id);
        $serie->isPublished = true;
        $serie->save();
        return redirect()->route('formations.index');
    }

     public function unpublish($id)
    {
        $serie              = Serie::findOrFail($id);
        $serie->isPublished = false;
        $serie->save();
        return redirect()->route('formations.index');
    }

    public function update(Request $request, $id)
    {
//        $request->validate([
//            'title'       => 'required',
//            'category_id' => 'required',
//            'description' => 'required',
//            'type'        => 'required'
//        ]);

        $serie              = Serie::findOrFail($id);
        $serie->isPublished = false;
        $image_url ='';

        if ($request->hasFile('image_url')) {
            Cloudder::delete($serie->image_url);
            $serie->image_url     = $request->file('image_url')->getClientOriginalName();
            $image_url            = uploadFile($request->image_url);
            $serie->image_url     = $image_url;
        }

        $serie->fill($request->except('image_url'))->save();
        Session::flash('success', 'Modifié avec Succès!');
        return redirect()->route('formations.index');
    }

    public function delete($id)
    {
        $serie      = Serie::findOrFail($id);
        $publicId   = $serie->image_url;
        $serie->delete();
        Cloudder::delete($publicId);
        Session::flash('success', 'Supprimé avec Succès!');
        return redirect()->route('formations.index');
    }

   /*
    // fontion de uploader les videos soit en site ou en conference
    public function upload(Request $request)
    {
         if($request->type == 'en site'){
           request()->file(serie)->store();
        }else{
            
        }


      
      Session::flash('success', 'Ajouter Avec Success !!!');
        return redirect()->route('formations.show ');  
    }
    */
}
