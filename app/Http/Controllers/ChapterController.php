<?php

namespace App\Http\Controllers;

use App\Chapter;
use App\Serie;
use Illuminate\Http\Request;
use Session;

class ChapterController extends Controller
{
    public function index($slug){
        $series   = Serie::all();
        $chapters = Chapter::all();
        $serie    =   Serie::where('slug',$slug)->first();
        return view('instructors.chapters.index', compact('series','chapters','serie'));
    }

    public function create($slug){
        $serie = Serie::where('slug',$slug)->first();
        return view('instructors.chapters.create', compact('serie'));
    }

    public function store(Request $request,$serie_id)
    {
        $this->validate($request,[
            'title' => 'required|unique:chapters'
        ]);
        Chapter::create([
            'title'     => $request->title,
            'slug'      => str_slug($request->title),
            'serie_id'  => $serie_id
        ]);

        $serie = Serie::find($serie_id);
        Session::flash('success', 'Creer Avec Success !!!');
        return redirect()->route('formations.show',['slug' => $serie->slug]);
    }

    public function edit($id)
    {
        $chapter = Chapter::find($id);
        return view('instructors.chapters.edit',compact('chapter'));

    }
    public function update(Request $request, $id)
    {
        $chapter        = Chapter::find($id);
        $chapter->title = $request->title;
        $chapter->slug  = str_slug($request->title);
        $chapter->save();
        Session::flash('success', 'Modifié avec Succès!');
        return redirect()->route('formations.show',['slug' => $chapter->serie->slug]);
    }

    public function destroy($id)
    {
        $chapter = Chapter::find($id);
        $chapter->delete();
        Session::flash('success', 'Supprimé avec Succès!');
        return redirect()->back();
    }
}
