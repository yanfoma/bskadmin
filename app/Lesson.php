<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lesson extends Model
{
    use SoftDeletes;
    protected $fillable = [ 'serie_id','title','slug','section_id','episode_number','is_completed','description','video_id',
        'video_url','video_length','chapter_id','uid','processed','lesson_id','processed_percentage'];

    public function chapter(){
        return $this->belongsTo('App\Chapter');

    }

    public function serie(){
        return $this->belongsTo('App\Serie');

    }
}


