<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Serie extends Model
{
    protected $fillable = ['title', 'slug', 'image_url', 'description','requirements','category_id',
        'type', 'user_id','isPublished','price'];

    protected $with =['category', 'user', 'lessons'];

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function lessons(){
        return $this->hasMany('App\Lesson')->orderBy('episode_number');
    }

    public function chapters(){

        return $this->hasMany('App\Chapter');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function isPublic(){

        return  $this->isPublished == false ? true : false;
    }

    public function type(){

        return  $this->type == 'enSite' ? "En Site" : "En Conference";
    }
    public function isPublished(){

       return  $this->isPublished == false ? 'Privé' : 'Public';
    }
}
