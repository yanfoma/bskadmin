<?php
/**
 * User: fabioued
 * Date: 10/29/18
 * Time: 15:09
 */

if (! function_exists('show_serie')) {
    function show_serie($serie)
    {
        echo " ";
    }
}

if (! function_exists('uploadFile')) {
     function uploadFile($image) {
        $image_name           = $image->getRealPath();
        $publicId             = $image->getClientOriginalName();
        Cloudder::upload($image_name, $publicId);
        list($width, $height) = getimagesize($image_name);
        $image_url            = Cloudder::secureShow(Cloudder::getPublicId(), ["width" => $width, "height" => $height]);
        return $image_url;
    }
}