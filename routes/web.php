<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->group(function (){
    Auth::routes();
});

Route::get('/', function()
{
    return view('auth.login');
});

Route::prefix('admin')->middleware('auth')->group(function () {

    Route::get('/', function()
    {
        return view('auth.login');
    });

     Route::get('/joins', function()
    {
        return view('instructors.series.create');
    });

    Route::get('tabeau-de-bord',[
        'uses' => 'HomeController@dashboard',
        'as'   => 'dashboard'
    ]);

    /*
   |--------------------------------------------------------------------------\
   |                                       Categories Routes                       \
   |--------------------------------------------------------------------------\
   */

    Route::get('toutes-les-catégories',[
        'uses' => 'CategoryController@index',
        'as'   => 'categories.index'
    ]);

    Route::get('catégories/ajouter-une-catégorie',[
        'uses' => 'CategoryController@create',
        'as'   => 'categories.create'
    ]);

    Route::post('catégories/store',[
        'uses' => 'CategoryController@store',
        'as'   => 'categories.store'
    ]);

    Route::get('catégorie/edit/{id}', [

        'uses' => 'CategoryController@edit',
        'as'   => 'categories.edit'
    ]);

    Route::post('catégories/update/{id}', [

        'uses' => 'CategoryController@update',
        'as'   => 'categories.update'
    ]);

    Route::get('catégories/delete/{id}', [

        'uses' => 'CategoryController@destroy',
        'as'   => 'categories.delete'
    ]);

    /*
   |--------------------------------------------------------------------------\
   |                                       Chapters Routes                       \
   |--------------------------------------------------------------------------\
   */

    Route::get('tous-les-chapitres',[
        'uses' => 'ChapterController@index',
        'as'   => 'chapters.index'
    ]);

    Route::get('formation/{slug}/chapitres/ajouter-un-chapitre',[
        'uses' => 'ChapterController@create',
        'as'   => 'chapters.create'
    ]);

    Route::post('formations/{serie_id}/chapitres/store',[
        'uses' => 'ChapterController@store',
        'as'   => 'chapters.store'
    ]);

    Route::get('chapitres/edit/{id}', [

        'uses' => 'ChapterController@edit',
        'as'   => 'chapters.edit'
    ]);

    Route::post('chapitres/update/{id}', [

        'uses' => 'ChapterController@update',
        'as'   => 'chapters.update'
    ]);

    Route::get('chapitres/delete/{id}', [
        'uses' => 'ChapterController@destroy',
        'as'   => 'chapters.delete'
    ]);

    /*
   |--------------------------------------------------------------------------\
   |                                       Formations Routes                       \
   |--------------------------------------------------------------------------\
   */

    Route::get('mes-formations',[
        'uses' => 'SerieController@index',
        'as'   => 'formations.index'
    ]);

    Route::get('créer-une-formation',[
        'uses' => 'SerieController@create',
        'as'   => 'formations.create'
    ]);

    Route::get('formations/{slug}',[
        'uses' => 'SerieController@show',
        'as'   => 'formations.show'
    ]);

    Route::post('formations/store',[
        'uses' => 'SerieController@store',
        'as'   => 'formations.store'
    ]);

    Route::get('formations/edit/{slug}', [
        'uses' => 'SerieController@edit',
        'as'   => 'formations.edit'
    ]);

    Route::get('formations/publish/{id}', [
        'uses' => 'SerieController@publish',
        'as'   => 'formations.publish'
    ]);

    Route::get('formations/unpublish/{id}', [
        'uses' => 'SerieController@unpublish',
        'as'   => 'formations.unpublish'
    ]);

    Route::post('formations/update/{id}', [
        'uses' => 'SerieController@update',
        'as'   => 'formations.update'
    ]);

    Route::get('formations/delete/{id}', [
        'uses' => 'SerieController@delete',
        'as'   => 'formations.delete'
    ]);

        
        /* methode de upload
    
     Route::post('upload',[
        'uses' => 'SerieController@upload',
        'as'   => 'formations.upload'
    ]);
    **/

    /*
   |--------------------------------------------------------------------------\
   |                                       Lessons Routes                     \
   |--------------------------------------------------------------------------\
   */
    Route::get('formations/{formation_slug}/chapitres/{chapter_slug}/touts-les-cours',[
        'uses' => 'LessonController@index',
        'as'   => 'lessons.index'
    ]);

    Route::get('formations/{formation_slug}/chapitres/{chapter_slug}/cours/créer-un-cours',[
        'uses' => 'LessonController@create',
        'as'   => 'lessons.create'
    ]);

    Route::get('cours/{slug}',[
        'uses' => 'LessonController@show',
        'as'   => 'lessons.show'
    ]);

    Route::post('formations/{formation_slug}/chapitres/{chapter_slug}/cours/store',[
        'uses' => 'LessonController@store',
        'as'   => 'lessons.store'
    ]);

    Route::get('formations/{formation_slug}/chapitres/{chapter_slug}/cours/edit/{slug}', [
        'uses' => 'LessonController@edit',
        'as'   => 'lessons.edit'
    ]);

    Route::post('formations/{formation_slug}/chapitres/{chapter_slug}/cours/update/{id}', [
        'uses' => 'LessonController@update',
        'as'   => 'lessons.update'
    ]);

    Route::get('formations/{formation_slug}/chapitres/{chapter_slug}/cours/delete/{id}', [
        'uses' => 'LessonController@delete',
        'as'   => 'lessons.delete'
    ]);
 
    Route::get('/home', 'HomeController@index')->name('home');



});

