<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeriesTable extends Migration
{
    public function up()
    {
        if ( !Schema::hasTable('series'))
        {
            Schema::create('series', function (Blueprint $table)
            {
                $table->increments('id');
                $table->text('title');
                $table->text('slug')->unique();
                $table->text('image_url')->nullable();
                $table->text('description');
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('series');
    }
}
