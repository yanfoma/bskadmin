<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryToSeriesTable extends Migration
{
    public function up()
    {
        if (! Schema::hasColumn('series', 'category_id'))
        {
            Schema::table('series', function (Blueprint $table)
            {
                $table->integer('category_id');
            });
        }
    }
    public function down()
    {
        Schema::table('series', function (Blueprint $table) {
            $table->dropColumn('category_id');
        });
    }
}
