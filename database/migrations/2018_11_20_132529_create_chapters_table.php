<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChaptersTable extends Migration
{
    public function up()
    {
        if (! Schema::hasTable('chapters'))
        {
            Schema::create('chapters', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('serie_id');
                $table->string('title');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('chapters');
    }
}
