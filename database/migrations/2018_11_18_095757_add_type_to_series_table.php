<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToSeriesTable extends Migration
{
    public function up()
    {
        if (! Schema::hasColumn('series', 'type'))
        {
            Schema::table('series', function (Blueprint $table)
            {
                $table->string('type');
            });
        }
    }
    public function down()
    {
        Schema::table('series', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
