<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdToSeriesTable extends Migration
{
    public function up()
    {
        if (! Schema::hasColumn('series', 'user_id'))
        {
            Schema::table('series', function (Blueprint $table)
            {
                $table->integer('user_id');
            });
        }
    }
    public function down()
    {
        Schema::table('series', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
    }
}
