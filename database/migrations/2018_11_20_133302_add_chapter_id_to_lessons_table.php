<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChapterIdToLessonsTable extends Migration
{
    public function up()
    {
        if (! Schema::hasColumn('lessons', 'chapter_id'))
        {
            Schema::table('lessons', function (Blueprint $table)
            {
                $table->integer('chapter_id');
            });
        }
    }
    public function down()
    {
        Schema::table('lessons', function (Blueprint $table) {
            $table->dropColumn('chapter_id');
        });
    }
}
