<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPreviewColumnToLessonsTable extends Migration
{
    public function up()
    {
        if (! Schema::hasColumn('lessons', 'preview'))
        {
            Schema::table('lessons', function (Blueprint $table)
            {
                $table->boolean('preview')->default(false);
            });
        }
    }
    public function down()
    {
        Schema::table('lessons', function (Blueprint $table) {
            $table->dropColumn('preview');
        });
    }
}
