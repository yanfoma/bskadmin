<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriceToSerieTables extends Migration
{
    public function up()
    {
        if (! Schema::hasColumn('series', 'price'))
        {
            Schema::table('series', function (Blueprint $table)
            {
                $table->integer('price');
            });
        }
    }
    public function down()
    {
        Schema::table('series', function (Blueprint $table) {
            $table->dropColumn('price');
        });
    }
}
