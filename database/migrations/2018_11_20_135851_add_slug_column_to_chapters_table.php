<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSlugColumnToChaptersTable extends Migration
{
    public function up()
    {
        if (! Schema::hasColumn('chapters', 'slug'))
        {
            Schema::table('chapters', function (Blueprint $table)
            {
                $table->string('slug')->unique();
            });
        }
    }
    public function down()
    {
        Schema::table('chapters', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
    }
}
