<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRequirementToSeriesTable extends Migration
{
    public function up()
    {
       if (! Schema::hasColumn('series', 'requirements'))
       {
           Schema::table('series', function (Blueprint $table)
           {
               $table->text('requirements')->nullable();
           });
       }
    }

    public function down()
    {
        Schema::table('series', function (Blueprint $table) {
            $table->dropColumn('requirements');
        });
    }
}
