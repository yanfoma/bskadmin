<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{

    public function up()
    {
        if (! Schema::hasTable('payments'))
        {
            Schema::create('payments', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('serie_id');
                $table->integer('user_id');
                $table->string('invoiceId')->nullable();
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
