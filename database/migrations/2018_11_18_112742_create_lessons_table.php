<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonsTable extends Migration
{
    public function up()
    {
        if (! Schema::hasTable('lessons'))
        {
            Schema::create('lessons', function (Blueprint $table)
            {
                $table->increments('id');
                $table->integer('serie_id');
                $table->string('title');
                $table->string('slug');
                $table->integer('section_id');
                $table->integer('episode_number');
                $table->boolean('is_completed');
                $table->text('description');
                $table->integer('video_id');
                $table->text('video_url');
                $table->integer('video_length');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('lessons');
    }
}
