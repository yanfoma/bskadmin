<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcessingToLessonsTable extends Migration
{
    public function up()
    {
        if (! Schema::hasColumn('lessons', 'price'))
        {
            Schema::table('lessons', function (Blueprint $table)
            {
                $table->string('uid');
                $table->boolean('processed')->default(false);
                $table->integer('lesson_id')->nullable();
                $table->integer('processed_percentage')->nullable();
                $table->softDeletes();
            });
        }
    }
    public function down()
    {
        Schema::table('lessons', function (Blueprint $table) {
            $table->dropColumn('uid');
            $table->dropColumn('processed');
            $table->dropColumn('lesson_id');
            $table->dropColumn('processed_percentage');
        });
    }
}
