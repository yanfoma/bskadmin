<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsPublishedToSeriesTable extends Migration
{
    public function up()
    {
        if (! Schema::hasColumn('series', 'isPublished'))
        {
            Schema::table('series', function (Blueprint $table)
            {
                $table->boolean('isPublished')->default(false);
            });
        }
    }
    public function down()
    {
        Schema::table('series', function (Blueprint $table) {
            $table->dropColumn('isPublished');
        });
    }
}
