<div class="dashboard_contents p-top-100 p-bottom-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="author-info author-info--dashboard">
                    <h1 class="info">{{$categories->count()}}</h1>
                    <p>Categories</p>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="author-info author-info--dashboard">
                    <h1 class="primary">{{$series->count()}}</h1>
                    <p>Formations</p>
                </div>
            </div>
            <!-- end /.col-lg-3 col-md-6 -->

            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="author-info author-info--dashboard">
                    <h1 class="secondary">{{$chapters->count()}}</h1>
                    <p>Chapitres</p>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="author-info author-info--dashboard">
                    <h1 class="danger">{{$lessons->count()}}</h1>
                    <p>Cours</p>
                </div>
            </div>
            <!-- end /.col-lg-3 col-md-6 -->
        </div>
        <!-- end /.row -->

        <div class="row">
            @if($categories->count())
                <div class="col-lg-12 col-md-12">
                <div class="dashboard_module recent_buyers">
                    <div class="dashboard__title">
                        <h4>Categories</h4>
                        <div class="loading">
                            <a href="{{route('categories.index')}}">
                                Voir <span class="lnr icon-refresh"></span>
                            </a>
                        </div>
                    </div>
                    <div class="dashboard__content">
                        <ul>
                            @foreach($categories as $categorie)
                                <li>
                                    <div class="single_buyer">
                                        <div class="buyer__thumb_title">
                                            <div class="title">
                                                <h6>{{$categorie->name}}</h6>
                                            </div>
                                        </div>
                                        <div class="price">
                                            <p>{{$categorie->series->count()}} Formation(s)</p>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            @endif
            @if($series->count())
                <div class="col-lg-12 col-md-12">
                        <div class="dashboard_module recent_comment">
                            <div class="dashboard__title">
                                <h4>Mes Formations</h4>
                                <div class="loading">
                                    <a href="{{route('formations.index')}}">
                                        voir <span class="lnr icon-refresh"></span>
                                    </a>
                                </div>
                            </div>

                            <div class="dashboard__content">
                                <div class="thread">
                                    <ul class="media-list thread-list">
                                        @foreach($series as $serie)
                                            <li class="single-thread">
                                            <div class="media">
                                                <div class="media-left prod_thumbnail">
                                                    <div class="prod_thumbnail">
                                                        <img src="{{$serie->image_url}}" alt="product thumbnail" width="200" height="130">
                                                    </div>
                                                </div>
                                                <div class="media-body">
                                                    <div class="media-content">
                                                        <div>
                                                            <div class="media-heading">
                                                                <div class="dflex">
                                                                    <a href="{{route('formations.show',['slug' => $serie->slug])}}">
                                                                        <h4>{{$serie->title}}</h4>
                                                                    </a>
                                                                    @if($serie->isPublished)
                                                                        <span class="comment-tag buyer">Public</span>
                                                                    @else
                                                                        <span class="comment-tag buyer" style="background: red;">Prive</span>
                                                                    @endif
                                                                </div>
                                                                <p>categorie: <a href="">{{$serie->category->name}}</a> Mode: <a href="#">{{$serie->type == 'enSite' ? 'En Site' : 'Conference'}}</a></p>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <a href="#" class="reply-link" style="color: deeppink">{{$serie->lessons->count()}} Cours</a>
                                                            <a href="#" class="reply-link">{{$serie->chapters->count()}} Chapitre(s) </a>
                                                            {{--<a href="#" class="reply-link">Reply</a>--}}
                                                        </div>
                                                    </div>
                                                    <p>{{$serie->description}}</p>
                                                </div>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
           @endif
        </div>
        <!-- end /.row -->
    </div>
    <!-- end /.container -->
</div>