<!--//////////////////// JS GOES HERE ////////////////-->
<script src="{{asset('js/vendor/jquery/jquery-1.12.3.js')}}"></script>
<script src="{{asset('js/vendor/jquery/popper.min.js')}}"></script>
<script src="{{asset('js/vendor/jquery/uikit.min.js')}}"></script>
<script src="{{asset('js/vendor/bootstrap.min.js')}}"></script>
<script src="{{asset('js/vendor/chart.bundle.min.js')}}"></script>
<script src="{{asset('js/vendor/grid.min.js')}}"></script>
<script src="{{asset('js/vendor/jquery-ui.min.js')}}"></script>
<script src="{{asset('js/vendor/jquery.barrating.min.js')}}"></script>
<script src="{{asset('js/vendor/jquery.countdown.min.js')}}"></script>
<script src="{{asset('js/vendor/jquery.counterup.min.js')}}"></script>
<script src="{{asset('js/vendor/jquery.easing1.3.js')}}"></script>
<script src="{{asset('js/vendor/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/vendor/select2.full.min.js')}}"></script>
<script src="{{asset('js/vendor/slick.min.js')}}"></script>
<script src="{{asset('js/vendor/tether.min.js')}}"></script>
<script src="{{asset('js/vendor/trumbowyg.min.js')}}"></script>
<script src="{{asset('js/vendor/venobox.min.js')}}"></script>
<script src="{{asset('js/vendor/waypoints.min.js')}}"></script>
<script src="{{asset('js/dashboard.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
<script src="{{asset('js/dropify.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        // Basic
        $('.dropify').dropify();

        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove:  'Supprimer',
                error:   'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('.dropify-event').dropify();

        drEvent.on('dropify.beforeClear', function(event, element){
            return confirm("Do you really want to delete \"" + element.filename + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element){
            alert('File deleted');
        });
    });
</script>