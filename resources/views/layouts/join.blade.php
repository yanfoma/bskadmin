<section class="cta">
	<div class="container">
		<div class="row">
			<!-- Start Section Title -->
			<div class="col-md-12">
				<div class="section-title">
					<h1>BSK Formateurs </h1>
				</div>
			</div>
			<!-- Ends: .col-md-12/Section Title -->

			<!-- CTA Single -->
			<div class="col-md-5 cta-single">
				<h3>Offrir Une Formation en Site</h3>
				<p>Vous etes passionne, vous voulez partager votre savoir sur le site , n'hesitez pas.</p>
				<a href="{{route('joins')}}" class="btn btn--lg btn-primary">Postulez la Formation</a>
			</div>
			<div class="col-md-2 cta-divider">
				<span>OU</span>
			</div>
			<div class="col-md-5 cta-single">
				<h3>Offrir Une Formation en Conference</h3>
				<p>Vous etes passionne, vous voulez partager votre savoir en live avec les participants,alors n'hesitez pas.</p>
				<a href="{{route('formations.create')}}" class="btn btn--lg btn-secondary">Postulez les information</a>
			</div>

		</div>
	</div>
</section>
