<!--================================
    Start Footer
=================================-->
<footer class="footer-area footer--light">
	<div class="mini-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="copyright-text">
						<p>Depuis &copy; 2018
							<a href="#">BSK connexion</a> Tous Droits Réservés.
							<a href="#">Yanfoma</a>
						</p>
					</div>
					<div class="go_top">
						<span class="icon-arrow-up"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<!--================================
    End Footer
=================================-->