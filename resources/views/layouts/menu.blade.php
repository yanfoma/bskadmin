<div class="menu-area">
	<div class="top-menu-area">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="menu-fullwidth">
						<div class="logo-wrapper">
							<div class="logo logo-top">
								<a href="{{route('dashboard')}}">BSK ADMIN</a>
							</div>
						</div>
						<div class="menu-container">
							<div class="d_menu">
								<nav class="navbar navbar-expand-lg mainmenu__menu">
									<button class="navbar-toggler" type="button" data-toggle="collapse"
									        data-target="#bs-example-navbar-collapse-1"
									        aria-controls="bs-example-navbar-collapse-1"
									        aria-expanded="false" aria-label="Toggle navigation">
										<span class="navbar-toggler-icon icon-menu"></span>
									</button>
									<!-- Collect the nav links, forms, and other content for toggling -->
									<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
										<ul class="navbar-nav">
											<li><a href="{{route('home')}}">Accueil</a></li>
											<li><a href="{{route('login')}}">Se Connecter</a></li>
											<li><a href="{{route('register')}}">S'enregistrer</a></li>
											<li><a href="{{route('dashboard')}}">Tableau</a></li>
										</ul>
									</div>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@guest
@else
	@if(!(\Request::is('/')))
		<section class="breadcrumb-area">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2 class="page-title text-center">Bienvenue {{\Illuminate\Support\Facades\Auth::user()->name}} </h2>
					</div>
				</div>
			</div>
		</section>
		<section class="dashboard-area">
			<div class="dashboard_menu_area">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<button class="menu-toggler d-md-none">
								<span class="icon-menu"></span>Menu
							</button>
							<ul class="dashboard_menu">
								<li class="{{\Request::is('admin/tabeau-de-bord') ?'active':''}}">
									<a href="{{route('dashboard')}}"><span class="lnr icon-home"></span>Accueil</a>
								</li>
								<li class="{{\Request::is('admin/mes-formations','admin/créer-une-formation') ?'active':''}}">
									<a href="{{route('formations.index')}}">
										<span class="lnr icon-settings"></span>Mes Formations</a>
								</li>
								<li class="{{\Request::is('admin/toutes-les-catégories','admin/catégories/ajouter-une-catégorie') ?'active':''}}">
									<a href="{{route('categories.index')}}">
										<span class="lnr icon-basket"></span>Catégories</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		@endif
@endguest