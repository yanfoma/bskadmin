<!--================================
    START Product Grid
=================================-->
<section class="product-grid p-bottom-100">
	<div class="container">
		<div class="row">
			<!-- Start .product-list -->
			<div class="col-md-12 product-list">
				<div class="row">
					{{--@foreach()--}}
						<!-- Start .col-md-4 -->
							<div class="col-lg-4 col-md-6">
								<div class="product-single latest-single">
									<div class="product-thumb">
										<figure>
											<img src="images/product1.png" alt="" class="img-fluid">
											<figcaption>
												<ul class="list-unstyled">
													<li>
														<a href="">
															<span class="icon-basket"></span>
														</a>
													</li>
													<li>
														<a href="">Live Demo</a>
													</li>
												</ul>
											</figcaption>
										</figure>
									</div>
									<!-- Ends: .product-thumb -->
									<div class="product-excerpt">
										<h5>
											<a href="">E-commerce Shopping Cart</a>
										</h5>
										<ul class="titlebtm">
											<li>
												<img class="auth-img" src="images/auth-img2.png" alt="author image">
												<p>
													<a href="#">Theme-Valley</a>
												</p>
											</li>
											<li class="product_cat">
												in
												<a href="#">WordPress</a>
											</li>
										</ul>
										<ul class="product-facts clearfix">
											<li class="price">$20</li>
											<li class="sells">
												<span class="icon-basket"></span>81
											</li>
											<li class="product-fav">
												<span class="icon-heart" title="Add to collection" data-toggle="tooltip"></span>
											</li>
											<li class="product-rating">
												<ul class="list-unstyled">
													<li class="stars">
                                                    <span>
                                                        <i class="fa fa-star"></i>
                                                    </span>
														<span>
                                                        <i class="fa fa-star"></i>
                                                    </span>
														<span>
                                                        <i class="fa fa-star"></i>
                                                    </span>
														<span>
                                                        <i class="fa fa-star"></i>
                                                    </span>
														<span>
                                                        <i class="fa fa-star"></i>
                                                    </span>
													</li>
												</ul>
											</li>
										</ul>
									</div>
									<!-- Ends: .product-excerpt -->
								</div>
								<!-- Ends: .product-single -->
							</div>
							<!-- Ends: .col-md-4 -->
					{{--@endforeach--}}
				</div>
				<!-- Start Pagination -->
				<nav class="pagination-default">
					<ul class="pagination">
						<li class="page-item">
							<a class="page-link" href="#" aria-label="Previous">
								<span aria-hidden="true"><i class="fa fa-long-arrow-left"></i></span>
								<span class="sr-only">Previous</span>
							</a>
						</li>
						<li class="page-item active"><a class="page-link" href="#">1</a></li>
						<li class="page-item"><a class="page-link" href="#">2</a></li>
						<li class="page-item"><a class="page-link" href="#">3</a></li>
						<li class="page-item disabled"><a class="page-link" href="#">...</a></li>
						<li class="page-item"><a class="page-link" href="#">10</a></li>
						<li class="page-item">
							<a class="page-link" href="#" aria-label="Next">
								<span aria-hidden="true"><i class="fa fa-long-arrow-right"></i></span>
								<span class="sr-only">Next</span>
							</a>
						</li>
					</ul>
				</nav>
				<!-- Ends: /pagination-default -->
			</div>
			<!-- Ends: .product-list -->
		</div>
	</div>
</section>
