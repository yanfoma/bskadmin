@extends('layouts.app')
@section('title')
    Catégories
@endsection
@section('content')
    <div class="dashboard_contents section--padding">
        <div class="container">
            @if($categories->count())
                <div class="col-md-12">
                    <div class="">
                        <div class="modules__content">
                            <div class="withdraw_module withdraw_history bg-white">
                                <div class="withdraw_table_header">
                                    <h4 class="text-center">Toutes Les Catégories</h4>
                                </div>
                                <a href="{{route('categories.create')}}" class="btn btn-lg btn-primary">Ajouter Une Catégorie</a>
                                <div class="table-responsive">
                                    <table class="table withdraw__table table-hover table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Nom</th>
                                            <th>Modifier</th>
                                            <th>Supprimer</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($categories as $category)
                                            <tr>
                                                <td>{{$category->name}}</td>
                                                <td><a class="btn btn-lg btn-warning" href="{{route('categories.edit',   ['id' => $category->id ])}}">Modifier</a></td>
                                                <td><a class="btn btn-lg btn-danger " href="{{route('categories.delete', ['id' => $category->id ])}}">Supprimer</a></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <nav class="pagination-default">
                                        <ul class="pagination">
                                            {{$categories->render()}}
                                        </ul>
                                    </nav>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            @else
                <div class="row">
                    <div class="col-md-8 offset-md-2 text-center">
                        <div class="alert alert-danger" role="alert" >
                            <strong>Ouupppssss!</strong> Aucune Catégorie Pour l'instant!!!!!
                        </div>
                        <p class="text-center">Veuillez ajouter une catégorie avant de pouvoir créer votre formation !!!</p>
                        <br>
                        <a href="{{route('categories.create')}}" class="btn btn-lg btn-primary">Ajouter Une Catégorie</a>
                    </div>
                    <!-- end /.col-md-12 -->
                </div>
                <!-- end /.row -->
            @endif
        </div>
        <!-- end /.container -->
    </div>
@endsection
