@extends('layouts.app')
@section('title')
    Modifier Une Catégorie
@endsection
@section('content')
    <div class="dashboard_contents section--padding">
        <div class="container">
            <form method="POST" action="{{ route('categories.update', ['id' => $category->id]) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="information_module">
                            <div class="toggle_title">
                                <h4 class="text-center">Modifier Une Catégorie</h4>
                            </div>
                            <div class="information__set">
                                <div class="information_wrapper form--fields row">
                                    <div class="col-md-12">
                                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                            <label for="name">Nom<sup>*</sup></label>
                                            <input  name="name" type="text" id="name" class="text_field" placeholder="Nom de votre catégorie" value="{{$category->name}}">
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <!-- end /.information_wrapper -->
                            </div>
                            <!-- end /.information__set -->
                        </div>
                        <!-- end /.information_module -->
                    </div><!-- ends: .col-md-12 -->
                    <div class="col-md-12">
                        <div class="dashboard_setting_btn">
                            <button type="submit" class="btn btn--md btn-primary">Ajouter</button>
                            <button type="reset" class="btn btn-md btn-danger">Annuler</button>
                        </div>
                    </div>
                    <!-- end /.col-md-12 -->
                </div>
                <!-- end /.row -->
            </form>
            <!-- end /form -->
        </div>
        <!-- end /.container -->
    </div>
@endsection
