@extends('layouts.app')
@section('title')
    Mes Formations
@endsection
@section('content')
        <div class="dashboard_contents section--padding">
            <div class="container">
                @if($categories->count())
                    @if($series->count())
                        <div class="col-md-12">
                            <div class="filter-bar dashboard_title_area clearfix filter-bar2">

                                <div class="dashboard__title">
                                    <h3>Mes Formations</h3>
                                </div>

                                <a href="{{route('formations.create')}}" class="btn btn-lg btn-primary">Ajouter Une Formation</a>

                                <div class="filter__items">
                                    <div class="filter__option filter--text"></div>

                                    {{--<div class="filter__option filter--select">--}}
                                    {{--<div class="select-wrap">--}}
                                    {{--<select name="price">--}}
                                    {{--<option value="low">Price : Low to High</option>--}}
                                    {{--<option value="high">Price : High to low</option>--}}
                                    {{--</select>--}}
                                    {{--<span class="lnr icon-arrow-down"></span>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                </div>
                                <!-- end /.pull-right -->
                            </div>
                            <!-- end /.filter-bar -->
                        </div>
                        <div class="row">
                            @foreach($series as $serie)
                                <div class="col-lg-6 col-md-6">
                                    <div class="product-single latest-single items--edit">
                                        <div class="product-thumb">
                                            <figure>
                                                <img src="{{asset($serie->image_url)}}" alt="" class="img-fluid">
                                                <div class="prod_option show">
                                                    <a href="#" id="drop2" class="dropdown-toggle" data-toggle="dropdown"
                                                       aria-haspopup="true" aria-expanded="true">
                                                        <span class="icon-settings setting-icon"></span>
                                                    </a>

                                                    <div class="options dropdown-menu" aria-labelledby="drop2">
                                                        <ul>
                                                            <li class="dropdown-item">
                                                                <a href="{{route('formations.edit',['slug' => $serie->slug])}}">
                                                                    <span class="icon-pencil"></span>Modifier</a>
                                                            </li>
                                                            @if($serie->isPublic())
                                                                <li class="dropdown-item">
                                                                    <a href="{{route('formations.publish',['id' => $serie->id])}}">
                                                                        <span class="icon-eye"></span>Publier</a>
                                                                </li>
                                                            @else
                                                                <li class="dropdown-item">
                                                                    <a href="{{route('formations.unpublish',['id' => $serie->id])}}">
                                                                        <span class="icon-eye"></span>Rendre Privé</a>
                                                                </li>
                                                            @endif
                                                            <li class="dropdown-item">
                                                                <a href="{{route('formations.delete',['id' => $serie->id])}}" class="delete">
                                                                    <span class="icon-trash"></span>Supprimer</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                        <div class="product-excerpt">
                                            <h5>
                                                <a href="">{{$serie->title}}</a>
                                            </h5>
                                            <ul class="titlebtm">
                                                <li>
                                                    <p>
                                                        Par
                                                        <a href="#">{{$serie->user->name}}</a>
                                                    </p>
                                                </li>
                                                <li class="product_cat"> Dans <a href="#">{{$serie->category->name}}</a></li>
                                            </ul>
                                            <ul class="product-facts clearfix">
                                                <li class="price">Prix: ${{$serie->price}}</li>
                                                @if($serie->isPublished)
                                                    <li class="price" style="background: green;">Status: {{$serie->isPublished()}}</li>
                                                @else
                                                    <li class="price" style="background: red;">Status: {{$serie->isPublished()}}</li>
                                                @endif
                                                @if($serie->type == 'enSite')
                                                    <li class="price">Mode: Site</li>
                                                @elseif($serie->type == 'enConference')
                                                    <li class="price">Mode: App</li>
                                                @endif
                                                {{--<li class="sells">--}}
                                                    {{--<span class="icon-basket"></span>81--}}
                                                {{--</li>--}}
                                                {{--<li class="product-fav">--}}
                                                    {{--<span class="icon-heart" title="Add to collection" data-toggle="tooltip"></span>--}}
                                                {{--</li>--}}
                                            </ul>
                                            <hr>
                                            <a href="{{route('formations.show',['slug' => $serie->slug])}}" class="btn btn-info">Voir</a>
                                        </div>
                                        <!-- Ends: .product-excerpt -->
                                    </div>
                                    <!-- Ends: .product-single -->
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="row">
                             <div class="col-md-8 offset-md-2 text-center">
                                 <div class="alert alert-primary" role="alert" >
                                     <strong>Aucune Formation Pour l'instant!!!!!</strong>
                                 </div>
                                 <a href="{{route('formations.create')}}" class="btn btn-lg btn-primary">Ajouter Une Formation</a>
                             </div>
                        </div>
                    @endif
                @else
                    <div class="row">
                         <div class="col-md-8 offset-md-2 text-center">
                            <div class="alert alert-danger" role="alert" >
                                <strong>Ouupppssss!</strong> Aucune Catégorie Pour l'instant!!!!!
                            </div>
                            <p class="text-center">Veuillez ajouter une catégorie avant de pouvoir créer votre formation !!!</p>
                            <br>
                            <a href="{{route('categories.create')}}" class="btn btn-lg btn-primary">Ajouter Une Catégorie</a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
@endsection
