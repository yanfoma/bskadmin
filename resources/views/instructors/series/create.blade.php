@extends('layouts.app')
@section('title')
    Créer Une Formation
@endsection
@section('content')
    <div class="dashboard_contents section--padding">
        <div class="container">
            <form method="POST" action="{{ route('formations.store') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="information_module">
                            <div class="toggle_title">
                                <h4 class="text-center">Créer Une Formation</h4>
                            </div>
                            <div class="information__set">
                                <div class="information_wrapper form--fields row">
                                    <div class="col-md-12">
                                        <div class="form-group{{ $errors->has('image_url') ? ' has-error' : '' }}">
                                            <label for="image_url">Image<sup>*</sup></label>
                                            <input name="image_url" type="file" id="input-file-events" class="dropify-event" value="{{ old('image_url') }}"/>
                                            @if ($errors->has('image_url'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('image_url') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                            <label for="title">Titre<sup>*</sup></label>
                                            <input  name="title" type="text" id="title" class="text_field" placeholder="First Name" value="{{old('title')}}">
                                            @if ($errors->has('title'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('title') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('prix') ? ' has-error' : '' }}">
                                            <label for="prix">Prix De Participation En Dollards</label>
                                            <div class="input-group">
                                                <input name="prix" type="number" id="prix" class="text_field" placeholder="1000" value="{{old('prix')}}">
                                                @if ($errors->has('prix'))
                                                    <span class="help-block">
                                                    <strong>{{ $errors->first('prix') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                                            <label for="category_id">Catégorie</label>
                                            <div class="select-wrap select-wrap2">
                                                <select name="category_id" id="category_id" class="text_field">
                                                    @if(old('category_id'))
                                                        <option value="{{ old('category_id') }}" selected disabled>{{ old('category_id') }}</option>
                                                    @endif
                                                    @foreach($categories as $category)
                                                        <option value="{{ $category->id }}"> {{ $category->name }} </option>
                                                    @endforeach
                                                </select>
                                                <span class="lnr icon-arrow-down"></span>
                                            </div>
                                            @if ($errors->has('category_id'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('category_id') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
                                            <label for="type">Type</label>
                                            <div class="select-wrap select-wrap2">
                                                <select name="type" id="type" class="text_field">
                                                    <option value="enSite"> En Site </option>
                                                    <option value="enConference"> En Conference </option>
                                                </select>
                                                <span class="lnr icon-arrow-down"></span>
                                            </div>
                                            @if ($errors->has('type'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('type') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <p class="label">Status</p>
                                            <div class="custom_checkbox checkbox-outline">
                                                <span class="check-confirm" data-text-swap="Public" data-text-original="Privé">Privé</span>
                                                <label class="toggle-switch">
                                                    <input type="checkbox" name="status">
                                                    <span class="slider round"></span>
                                                </label>
                                            </div><!-- end /.custom-checkbox -->
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                            <label for="description">Description</label>
                                            <textarea name="description" id="description" class="text_field" placeholder="Breve description de votre formation...">{{ old('description') }}</textarea>
                                            @if ($errors->has('description'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('description') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group{{ $errors->has('requirements') ? ' has-error' : '' }}">
                                            <label for="requirements">Prérequis</label>
                                            <textarea name="requirements" id="requirements" class="text_field" placeholder="Prérequis?">{{ old('requirements') }}</textarea>
                                            @if ($errors->has('requirements'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('requirements') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="dashboard_setting_btn">
                            <button type="submit" class="btn btn--md btn-primary">Ajouter</button>
                            <button type="reset" class="btn btn-md btn-danger">Annuler</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
