@extends('layouts.app')
@section('title')
    Formation: {{$serie->title}}
@endsection
@section('content')
    <section class="dashboard-area dashboard_purchase">
        <div class="dashboard_contents section--padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="filter-bar clearfix filter-bar2 p-bottom-30 p-right-30">
                            <div class="col-md-4">
                                <div class="dashboard__title">
                                    <h3>Formation:</h3> <h3 class="primary">{{$serie->title}}</h3>
                                </div>
                            </div>
                            @if($serie->chapters->count())
                                <div class="col-md-4">
                                    <div class="dashboard__title">
                                        <a href="{{route('chapters.create',['slug' => $serie->slug ])}}" class="btn btn-sm btn-primary text-center">Ajouter un Chapitre</a>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="dashboard__title">
                                        @if($serie->chapters->count())
                                            <h3>{{$serie->chapters->count()}}  Chapitre(s)</h3>
                                        @else
                                            <h3>Aucun Chapitre disponible!!! </h3>
                                        @endif
                                    </div>
                                </div>
                                <div class="table-responsive ">
                                    <table class="table withdraw__table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Titre</th>
                                                <th>Cours</th>
                                                <th>Voir Cours</th>
                                                <th>Modifier</th>
                                                <th>Supprimer</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($serie->chapters as $chapter)
                                            <tr>
                                                <td>{{$chapter->title}}</td>
                                                <td>{{$chapter->lessons->count()}}</td>
                                                <td><a class="btn btn-sm btn-info"    href="{{route('lessons.index',   ['formation_slug' => $serie->slug, 'chapter_slug' => $chapter->slug ])}}">Voir</a></td>
                                                <td><a class="btn btn-sm btn-warning" href="{{route('chapters.edit',   ['id' => $chapter->id ])}}">Modifier</a></td>
                                                <td><a class="btn btn-sm btn-danger " href="{{route('chapters.delete', ['id' => $chapter->id ])}}">Supprimer</a></td>
                                            </tr>
                                        @endforeach
                                        </tbody> 
                                    </table>
                                </div>
                            @else
                                <div class="col-md-4">
                                    <div class="dashboard__title">
                                        <a href="{{route('chapters.create',['slug' => $serie->slug ])}}" class="btn btn-sm btn-primary text-center">Ajouter un chapitre</a>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="dashboard__title">
                                        <h3>{{$serie->lessons->count() == 0 ? "Aucun Chapitre disponible !!! " : $serie->lessons->count()+" Chapitre !!!" }}</h3>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
