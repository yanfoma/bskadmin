@extends('layouts.app')
@section('title')
    Formation: {{$serie->title}}
@endsection
@section('content')
    <section class="dashboard-area dashboard_purchase">
        <div class="dashboard_contents section--padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="filter-bar clearfix filter-bar2">
                            <div class="col-md-4">
                                <div class="dashboard__title">
                                    <h3>Formation: <a href="{{route('formations.show',['slug' => $serie->slug])}}" >{{$serie->title}}</a> <br></h3>
                                    <h3>Chapitre:  <a href="{{route('formations.show',['slug' => $serie->slug ])}}">{{$chapter->title}}</a></h3>
                                </div>
                            </div>
                            @if($chapter->lessons->count())
                                <div class="col-md-4">
                                    <div class="dashboard__title">
                                        <a href="{{route('lessons.create',['formation_slug' => $serie->slug, 'chapter_slug' => $chapter->slug ])}}" class="btn btn-sm btn-primary text-center">Ajouter Un Cours</a>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="dashboard__title">
                                        @if($chapter->lessons->count())
                                            <h3>{{$chapter->lessons->count()}}  Cours(s)</h3>
                                        @else
                                            <h3>Aucun Cours !!! </h3>
                                        @endif
                                    </div>
                                </div>
                                <div class="table-responsive p-bottom-30 p-right-30">
                                    <table class="table withdraw__table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Episode</th>
                                                <th>Titre</th>
                                                <th>Formation</th>
                                                <th>Chapitre</th>
                                                <th>Voir Cours</th>
                                                <th>Modifier</th>
                                                <th>Supprimer</th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                @foreach($chapter->lessons as $lesson)
                                                    <tr>
                                                        <td>{{$lesson->episode_number}}</td>
                                                        <td>{{$lesson->title}}</td>
                                                        <td>{{$lesson->serie->title}}</td>
                                                        <td>{{$lesson->chapter->title}}</td>
                                                        <td><a class="btn btn-sm btn-info"    href="{{route('lessons.index',   ['formation_slug' => $serie->slug, 'chapter_slug' => $chapter->slug ])}}">Voir</a></td>
                                                        <td><a class="btn btn-sm btn-warning" href="{{route('lessons.edit',    ['formation_slug' => $serie->slug, 'chapter_slug' => $chapter->slug ,'slug' => $lesson->slug])}}">Modifier</a></td>
                                                        <td><a class="btn btn-sm btn-danger " href="{{route('lessons.delete',  ['formation_slug' => $serie->slug, 'chapter_slug' => $chapter->slug ,'id' => $lesson->id])}}">Supprimer</a></td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                    </table>
                                </div>
                            @else
                                <div class="col-md-4">
                                    <div class="dashboard__title">
                                        <a href="{{route('lessons.create',['formation_slug' => $serie->slug, 'chapter_slug' => $chapter->slug ])}}" class="btn btn-sm btn-primary text-center">Ajouter Un Cours</a>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="dashboard__title">
                                        <h3>{{$chapter->lessons->count() == 0 ? "Aucun Cours !!! " : $chapter->lessons->count() + " Cours !!!" }}</h3>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
