@extends('layouts.app')
@section('title')
    Créer Un Cours
@endsection
@section('content')
    <div class="dashboard_contents section--padding">
        <div class="container">
            <form method="POST" action="{{ route('lessons.store',['formation_slug' => $serie->slug, 'chapter_slug' => $chapter->slug ]) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="information_module">
                            <div class="toggle_title">
                                <h4 class="text-center">Créer Un Cours</h4>
                            </div>
                            <div class="information__set">
                                <div class="information_wrapper form--fields row">
                                    {{--<div class="col-md-12">--}}
                                        {{--<div class="form-group{{ $errors->has('video_url') ? ' has-error' : '' }}">--}}
                                            {{--<label for="video_url">Video<sup>*</sup></label>--}}
                                            {{--<input name="video_url" type="file" id="input-file-events" class="dropify-event" value="{{ old('video_url') }}"/>--}}
                                            {{--@if ($errors->has('video_url'))--}}
                                                {{--<span class="help-block">--}}
                                                    {{--<strong>{{ $errors->first('video_url') }}</strong>--}}
                                                {{--</span>--}}
                                            {{--@endif--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('video_url') ? ' has-error' : '' }}">
                                            <label for="video_url">Video<sup>*</sup></label>
                                            <input  name="video_url" type="file" id="video_url" class="text_field" placeholder="Video du cours" value="{{old('video_url')}}" required>
                                            @if ($errors->has('video_url'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('video_url') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                            <label for="title">Titre<sup>*</sup></label>
                                            <input  name="title" type="text" id="title" class="text_field" placeholder="Titre du cours" value="{{old('title')}}" required>
                                            @if ($errors->has('title'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('title') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('episode_number') ? ' has-error' : '' }}">
                                            <label for="episode_number">Numero du cours <sup>*</sup></label>
                                            <input  name="episode_number" type="text" id="episode_number" class="text_field" placeholder="Numero" value="{{old('episode_number')}}" required>
                                            @if ($errors->has('episode_number'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('episode_number') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('chapter') ? ' has-error' : '' }}">
                                            <label for="chapter">Chapitre Correspondant</label>
                                            <input  name="chapter" type="text" id="chapter" class="text_field" value="{{$chapter->title}}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('formation') ? ' has-error' : '' }}">
                                            <label for="formation">Formation Correspondante</label>
                                            <input  name="formation" type="text" id="formation" class="text_field" value="{{$serie->title}}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('previsualiser') ? ' has-error' : '' }}">
                                            <p class="label">Previsualiser</p>
                                            <div class="custom_checkbox checkbox-outline">
                                                <span class="check-confirm" data-text-swap="Oui" data-text-original="Non">Non</span>
                                                <label class="toggle-switch">
                                                    <input type="checkbox" name="previsualiser">
                                                    <span class="slider round"></span>
                                                </label>
                                                @if ($errors->has('previsualiser'))
                                                    <span class="help-block">
                                                    <strong>{{ $errors->first('previsualiser') }}</strong>
                                                </span>
                                                @endif
                                            </div><!-- end /.custom-checkbox -->
                                        </div>
                                    </div>
                                </div>
                                <!-- end /.information_wrapper -->
                            </div>
                            <!-- end /.information__set -->
                        </div>
                        <!-- end /.information_module -->
                    </div><!-- ends: .col-md-12 -->
                    <div class="col-md-12">
                        <div class="dashboard_setting_btn">
                            <button type="submit" class="btn btn--md btn-primary">Ajouter</button>
                            <button type="reset" class="btn btn-md btn-danger">Annuler</button>
                        </div>
                    </div>
                    <!-- end /.col-md-12 -->
                </div>
                <!-- end /.row -->
            </form>
            <!-- end /form -->
        </div>
        <!-- end /.container -->
    </div>
@endsection
