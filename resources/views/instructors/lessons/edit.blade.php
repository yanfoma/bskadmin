@extends('layouts.app')
@section('title')
    Modifier Une Formation
@endsection
@section('content')
    <div class="dashboard_contents section--padding">
        <div class="container">
            <form method="POST" action="{{ route('formations.update',['id' => $serie->id]) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="information_module">
                            <div class="toggle_title">
                                <h4 class="text-center">Modifier Une Formation</h4>
                            </div>
                            <div class="information__set">
                                <div class="information_wrapper form--fields row">
                                    <div class="col-md-12">
                                        <div class="form-group{{ $errors->has('image_url') ? ' has-error' : '' }}">
                                            <label for="image_url">Image<sup>*</sup></label>
                                            <input name="image_url" type="file" id="input-file-events" class="dropify-event" data-default-file="{{asset($serie->image_url)}}" value="{{$serie->image_url}}"/>
                                            @if ($errors->has('image_url'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('image_url') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                            <label for="title">Titre<sup>*</sup></label>
                                            <input  name="title" type="text" id="title" class="text_field" placeholder="First Name" value="{{ $serie->title }}">
                                            @if ($errors->has('title'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('title') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                                            <label for="category_id">Categorie</label>
                                            <div class="select-wrap select-wrap2">
                                                <select name="category_id" id="category_id" class="text_field">
                                                    @if($serie->category_id)
                                                        <option value="{{ $serie->category_id }}" selected disabled>{{ $serie->category->name }}</option>
                                                    @endif
                                                    @foreach($categories as $category)
                                                        <option value="{{ $category->id }}"> {{ $category->name }} </option>
                                                    @endforeach
                                                </select>
                                                <span class="lnr icon-arrow-down"></span>
                                            </div>
                                            @if ($errors->has('category_id'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('category_id') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div><!-- end:.col-md-6 -->
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                            <label for="type">Type</label>
                                            <div class="select-wrap select-wrap2">
                                                <select name="type" id="type" class="text_field">
                                                    @if($serie->type)
                                                        <option value="{{ $serie->type }}" selected disabled>{{ $serie->type }}</option>
                                                    @endif
                                                    <option value="enSite"> En Site </option>
                                                    <option value="enConference"> En Conference </option>
                                                </select>
                                                <span class="lnr icon-arrow-down"></span>
                                            </div>
                                            @if ($errors->has('type'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('type') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div><!-- end:.col-md-6 -->
                                    <div class="col-md-12">
                                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                            <label for="description">Description</label>
                                            <textarea name="description" id="description" class="text_field" placeholder="Breve description de votre formation...">{{ $serie->description }}</textarea>
                                            @if ($errors->has('description'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('description') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group{{ $errors->has('requirements') ? ' has-error' : '' }}">
                                            <label for="requirements">Prérequis</label>
                                            <textarea name="requirements" id="requirements" class="text_field" placeholder="Prérequis?">{{ $serie->requirements }}</textarea>
                                            @if ($errors->has('requirements'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('requirements') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <!-- end /.information_wrapper -->
                            </div>
                            <!-- end /.information__set -->
                        </div>
                        <!-- end /.information_module -->
                    </div><!-- ends: .col-md-12 -->
                    <div class="col-md-12">
                        <div class="dashboard_setting_btn">
                            <button type="submit" class="btn btn--md btn-primary">Sauvegarder</button>
                            <button type="reset" class="btn btn-md btn-danger">Annuler</button>
                        </div>
                    </div>
                    <!-- end /.col-md-12 -->
                </div>
                <!-- end /.row -->
            </form>
            <!-- end /form -->
        </div>
        <!-- end /.container -->
    </div>
@endsection
