@extends('layouts.app')
@section('title')
    Formation: {{$serie->title}}
@endsection
@section('content')
    <section class="dashboard-area dashboard_purchase">
        <div class="dashboard_contents section--padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="filter-bar clearfix filter-bar2">
                            <div class="col-md-4">
                                <div class="dashboard__title">
                                    <h3>Formation: {{$serie->title}}</h3>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="dashboard__title">
                                    <a href="{{route('lessons.create')}}" class="btn btn-sm btn-primary text-center">Créer Un Cours</a>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="dashboard__title">
                                    <h3>{{$serie->lessons->count() == 0 ? "Aucun Cours disponible !!! " : $serie->lessons->count()+" Cours" }}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @forelse($serie->lessons as $lesson)
                    <div class="product_archive">
                    <div class="title_area">
                        <div class="row">
                            <div class="col-md-5 col-sm-6">
                                <h5>Titre</h5>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <h5 class="add_info">Durée</h5>
                            </div>
                            <div class="col-md-2 col-sm-6">
                                <h5>Modifier</h5>
                            </div>
                            <div class="col-md-2 col-sm-6">
                                <h5>Supprimer</h5>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="single_product clearfix col-md-12">
                            <div class="row">
                                <div class="col-lg-5 col-sm-12">
                                    <div class="product__description">
                                        <div class="short_desc">
                                            <h5>
                                                <a href="">{{$lesson->title}}</a>
                                            </h5>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6">
                                    <div class="product__price_download">
                                        <div class="item_price v_middle">
                                            <span>{{$lesson->video_length}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-6">
                                    <div class="product__price_download">
                                        <a href="{{route('lessons.edit',['id' => $lesson->id])}}" class="btn btn-sm btn-warning">Modifier</a>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-6">
                                    <div class="product__price_download">
                                        <a href="{{route('lessons.delete',['id' => $lesson->id])}}" class="btn btn-sm btn-danger">Supprimer</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <nav class="pagination-default">
                            </nav>
                        </div>
                    </div>
                </div>
                @empty
                @endforelse
            </div>
        </div>
    </section>
@endsection
