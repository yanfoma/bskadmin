@extends('layouts.app')
@section('title')
    Chapitres De La Formation Sur :
@endsection
@section('content')
    <div class="dashboard_contents section--padding">
        <div class="container">
            @if($series->count())
                <div class="col-md-12">
                    <div class="">
                        <div class="modules__content">
                            <div class="withdraw_module withdraw_history bg-white">
                                <div class="withdraw_table_header">
                                    <h4 class="text-center">Tous Les Chapitres</h4>
                                </div>
                                <a href="{{route('chapters.create',['slug' => $serie->slug])}}" class="btn btn-lg btn-primary">Ajouter Un Chapitre</a>
                                <div class="table-responsive">
                                    <table class="table withdraw__table table-hover table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Nom</th>
                                            <th>Modifier</th>
                                            <th>Supprimer</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($chapters as $chapter)
                                            <tr>
                                                <td>{{$chapter->title}}</td>
                                                <td><a class="btn btn-sm btn-warning" href="{{route('chapters.edit',   ['id' => $chapter->id ])}}">Modifier</a></td>
                                                <td><a class="btn btn-sm btn-danger " href="{{route('chapters.delete', ['id' => $chapter->id ])}}">Supprimer</a></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <nav class="pagination-default">
                                        <ul class="pagination">
                                            {{$chapters->render()}}
                                        </ul>
                                    </nav>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-md-8 offset-md-2 text-center">
                        <div class="alert alert-danger" role="alert" >
                            <strong>Ouupppssss!</strong> Aucune Formation Pour l'instant!!!!!
                        </div>
                        <p class="text-center">Veuillez ajouter une formations avant de pouvoir créer vos chapitres !!!</p>
                        <br>
                        <a href="{{route('chapters.create',['slug' => $serie->slug])}}" class="btn btn-lg btn-primary">Ajouter Un Chapitre</a>
                    </div>
                    <!-- end /.col-md-12 -->
                </div>
                <!-- end /.row -->
            @endif
        </div>
        <!-- end /.container -->
    </div>
@endsection
