@extends('layouts.app')
@section('title')
    {{$chapter->title}} De La Formation {{$chapter->serie->title}}
@endsection
@section('content')
    <div class="dashboard_contents section--padding">
        <div class="container">
            <form method="POST" action="{{ route('chapters.update', ['id' => $chapter->id]) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="information_module">
                            <div class="toggle_title">
                                <h4 class="text-center">{{$chapter->title}} De La Formation {{$chapter->serie->title}}</h4>
                            </div>
                            <div class="information__set">
                                <div class="information_wrapper form--fields row">
                                    <div class="col-md-12">
                                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                            <label for="title">Titre de Votre Chapitre<sup>*</sup></label>
                                            <input  name="title" type="text" id="title" class="text_field" placeholder="Titre de votre chapitre" value="{{$chapter->title}}">
                                            @if ($errors->has('title'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('title') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="dashboard_setting_btn">
                            <button type="submit" class="btn btn--md btn-primary">Ajouter</button>
                            <button type="reset" class="btn btn-md btn-danger">Annuler</button>
                        </div>
                    </div>
                    <!-- end /.col-md-12 -->
                </div>
                <!-- end /.row -->
            </form>
            <!-- end /form -->
        </div>
        <!-- end /.container -->
    </div>
@endsection
